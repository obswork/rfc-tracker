# RFC Tracker

RFC Tracker is a UI for uploading, tracking the status of, and exploring RFCs.

Built with:
- [11ty](https://www.11ty.io/)
- [Sass/SCSS](https://github.com/sass/node-sass)
- [Webpack](https://webpack.js.org/)
- [Babel](https://babeljs.io/)
- [PostCSS](https://postcss.org/)
- [CSSnano](https://cssnano.co/)
- [Autoprefixer](https://github.com/postcss/autoprefixer)


# Usage

1. Clone

   ```
   $ git clone git@gitlab.com:obswork/rfc-tracker.git
   ```

2. Use supported version of node

   At this time, this project supports node versions >= v14. Using nvm:

   ```
   $ nvm use
   ```

   If not installed, install first:

   ```
   $ nvm install 14
   ```

3. Install dependencies

   ..oh wait, we're using Yarn [Zero-Installs](https://yarnpkg.com/features/zero-installs) so 
   dependencies are cached in version-control..kinda. 

   To refresh/rebuild local cache:

   ```
   $ yarn
   ```

4. Build for local development and serve

   ```
   $ yarn dev
   ```

5. Build for production

   ```
   $ yarn build
   ```
