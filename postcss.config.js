const tailwindcss = require('tailwindcss')
const cssnano = require('cssnano')
const autoprefixer = require('autoprefixer')

const plugins = [
  autoprefixer,
  tailwindcss('./tailwind.config.js'),
]

if (process.env.NODE_ENV === 'production') {
  plugins.push(require("cssnano"));
}

module.exports = {
  plugins
}
