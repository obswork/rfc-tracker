module.exports = function(eleventyConfig) {

  eleventyConfig.addPassthroughCopy('src/images');

  /* Ensure scripts are watched */
  eleventyConfig.addWatchTarget("./src/scripts/");

  return {
    dir: { input: 'src', output: 'dist', data: '_data' },
    passthroughFileCopy: true,
    htmlTemplateEngine: 'njk',
    templateFormats: ['njk', 'md', 'css', 'html', 'yml']
  }
}
